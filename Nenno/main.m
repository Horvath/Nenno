//
//  main.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 08..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
