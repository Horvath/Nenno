//
//  NennoHomeViewController.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 08..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoHomeViewController.h"

// UI Components
#import "NennoPizzaItemTableViewCell.h"
#import "NennoPizzaItemDetailsViewController.h"

// Helpers
#import <ObjectiveSugar/ObjectiveSugar.h>

// ViewModel
#import "NennoPizzaItem.h"

static CGFloat const kPizzasEstimatedRowHeight = 178.0f;

@interface NennoHomeViewController () <UITableViewDelegate, UITableViewDataSource>

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UITableView *pizzaItemsTableView;

#pragma mark - DataSource
@property (nonatomic, strong) NSArray<NennoPizzaItem *> *pizzaItems;

@end

@implementation NennoHomeViewController 

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupPizzaItemsTableView];
    [self loadPizzas];
}

#pragma mark - Setups
- (void)setupPizzaItemsTableView
{
    self.pizzaItemsTableView.rowHeight = UITableViewAutomaticDimension;
    self.pizzaItemsTableView.estimatedRowHeight = kPizzasEstimatedRowHeight;
}

#pragma mark - Fetch Data
- (void)loadPizzas
{
    [self presentLoading];

    [self.nennoDataPresenter loadPizzas:^(NSArray<NennoPizzaItem *> *pizzaItems)
     {
         [self hideLoading];
         self.pizzaItems = pizzaItems;
         [self.pizzaItemsTableView reloadData];
     }
                             errorBlock:^(NSError *error)
     {
         [self hideLoading];
         [self handleError:error];
     }];
}

#pragma mark - UITableView Delegate / DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.pizzaItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NennoPizzaItemTableViewCell *pizzaItemCell =
    [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([NennoPizzaItemTableViewCell class])];

    [pizzaItemCell setupWithPizzaItem:_pizzaItems[indexPath.row]];

    [pizzaItemCell setAddToCartClicked:^(id sender)
    {
        NennoPizzaItem *pizzaItem = _pizzaItems[indexPath.row];
        [pizzaItem resetIngredientsToDefault];

        [self.nennoDataPresenter addPizzaItemToCart:pizzaItem];
        [self.nennoDataPresenter persistCart];

        [self showSnackBar];
    }];

    return pizzaItemCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"NennoDetailsSegue" sender:_pizzaItems[indexPath.row]];
}

#pragma mark - Perform Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NennoPizzaItem *)sender
{
    if ([[segue identifier] isEqualToString:@"NennoDetailsSegue"])
    {
        NennoPizzaItemDetailsViewController *detailsVC = [segue destinationViewController];

        detailsVC.pizzaItem = sender;
    }
}

#pragma mark - Actions
- (IBAction)createClicked:(id)sender
{
    NennoPizzaItem *customPizzaItem = [NennoPizzaItem customPizzaItem];

    [self performSegueWithIdentifier:@"NennoDetailsSegue" sender:customPizzaItem];
}

@end
