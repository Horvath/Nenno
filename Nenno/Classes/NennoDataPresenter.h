//
//  NennoDataPresenter.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NennoDataPresenterInterface.h"

@interface NennoDataPresenter : NSObject <NennoDataPresenterInterface>

@end
