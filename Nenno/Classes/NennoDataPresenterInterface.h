//
//  NennoDataPresenterInterface.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

// ViewModel
@class NennoIngredient;
@class NennoPizzaItem;
@class NennoDrinkItem;
@protocol NennoCartItemProtocol;

@protocol NennoDataPresenterInterface <NSObject>

#pragma mark - Data
@property (nonatomic, strong) NSArray<NennoIngredient *> *ingredients;
@property (nonatomic, strong) NSArray<NennoDrinkItem *> *drinks;
@property (nonatomic, strong) NSMutableArray<id<NennoCartItemProtocol>> *cart;
@property (nonatomic, strong) NSNumber *cartTotalPrice;

#pragma mark - Fetch and Post aata
- (void)loadPizzas:(void (^)(NSArray<NennoPizzaItem *> *))successBlock
        errorBlock:(void (^)(NSError *))errorBlock;

- (void)checkout:(void (^)())successBlock
      errorBlock:(void (^)(NSError *))errorBlock;

#pragma mark - Utils
- (void)persistCart;
- (void)addPizzaItemToCart:(NennoPizzaItem *)pizzaItem;
- (void)addDrinkItemToCart:(NennoDrinkItem *)drinkItem;
- (void)deleteCartItemFromCart:(NSInteger)index;

@end
