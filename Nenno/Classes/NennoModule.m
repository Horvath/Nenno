//
//  NennoModule.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoModule.h"

// DataPresenters
#import "NennoDataPresenter.h"
#import "NennoMockDataPresenter.h"

// Components
#import "AppDelegate.h"

@implementation NennoModule

- (id<NennoDataPresenterInterface>)nennoDataPresenter
{
#ifdef MOCK
    return [TyphoonDefinition withClass:[NennoMockDataPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              definition.scope = TyphoonScopeSingleton;
                          }];
#else
    return [TyphoonDefinition withClass:[NennoDataPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              definition.scope = TyphoonScopeSingleton;
                          }];
#endif
}

- (AppDelegate *)appDelegate
{
    return [TyphoonDefinition withClass:[AppDelegate class]];
}

@end
