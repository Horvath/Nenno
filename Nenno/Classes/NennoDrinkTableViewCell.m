//
//  NennoDrinkTableViewCell.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoDrinkTableViewCell.h"

#import "NennoDrinkItem.h"

@interface NennoDrinkTableViewCell ()

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UILabel *dringTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *drinkPriceLabel;

@end

@implementation NennoDrinkTableViewCell

#pragma mark - Setups
- (void)setupWithDrink:(NennoDrinkItem *)drinkItem
{
    self.dringTitleLabel.text = drinkItem.name;
    self.drinkPriceLabel.text =
    [NSString stringWithFormat:@"$%ld", drinkItem.price.integerValue];
}

#pragma mark - Actions
- (IBAction)addDrinkClicked:(id)sender
{
    if (_addItemClicked)
    {
        _addItemClicked(sender);
    }
}

@end
