//
//  NennoDrinkItem.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoDrinkItem.h"

@implementation NennoDrinkItem

#pragma mark - JSONModel Methods
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{
              @"price"   : @"price",
              @"name"    : @"name",
              @"drinkId" : @"id",
              }];
}

#pragma mark - NennoCartItemProtocol Methods
- (NSString *)cartItemName
{
    return self.name;
}

- (NSNumber *)cartItemPrice
{
    return self.price;
}

#pragma mark - NSCoding Methods
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.price forKey:@"price"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.drinkId forKey:@"drinkId"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        self.price = [decoder decodeObjectForKey:@"price"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.drinkId = [decoder decodeObjectForKey:@"drinkId"];
    }
    return self;
}

@end
