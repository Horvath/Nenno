//
//  NennoDrinkTableViewCell.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NennoDrinkItem.h"

@interface NennoDrinkTableViewCell : UITableViewCell

@property (nonatomic, copy) void (^addItemClicked)(id);

- (void)setupWithDrink:(NennoDrinkItem *)drinkItem;

@end
