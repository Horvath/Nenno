//
//  NennoBaseViewController.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 08..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoBaseViewController.h"

// Helpers
#import "UIColor+NennoColor.h"
#import "CWStatusBarNotification.h"
#import <MRProgress/MRProgress.h>

@interface NennoBaseViewController ()

@property (nonatomic, strong) CWStatusBarNotification *statusBarNotification;

@end

@implementation NennoBaseViewController

#pragma mark - Life Cysle
- (void)viewDidLoad
{
    self.nennoDataPresenter = [self.nennoModule nennoDataPresenter];

    [self setupSnackBar];
}

- (void)setupSnackBar
{
    self.statusBarNotification = [CWStatusBarNotification new];
    _statusBarNotification.notificationLabelBackgroundColor =
    [UIColor nennoRed];
    _statusBarNotification.notificationLabelTextColor = [UIColor whiteColor];
    _statusBarNotification.notificationLabel.font =
    [UIFont fontWithName:@"SFUIText-Semibold" size:8.0f];
    _statusBarNotification.notificationAnimationInStyle = CWNotificationAnimationStyleTop;
    _statusBarNotification.notificationAnimationOutStyle = CWNotificationAnimationStyleTop;
}

#pragma mark - Common UI Actions
- (void)showSnackBar
{
    [self.statusBarNotification displayNotificationWithMessage:@"ADDED TO CART"
                                          forDuration:1.0f];
}

- (void)handleError:(NSError *)error
{
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:@"Error!"
                                        message:error.description
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleCancel
                                                     handler:nil];
    [alert addAction:okAction];

    [self presentViewController:alert
                       animated:YES
                     completion:nil];
}

- (void)presentLoading
{
    [MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
}

- (void)hideLoading
{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
}

#pragma mark - Actions
- (IBAction)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
