//
//  NennoPizzaItemTableViewCell.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NennoPizzaItem;

@interface NennoPizzaItemTableViewCell : UITableViewCell

@property (nonatomic, copy) void (^addToCartClicked)(id);

- (void)setupWithPizzaItem:(NennoPizzaItem *)pizzaItem;

@end
