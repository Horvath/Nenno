//
//  NennoIngredient.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoIngredient.h"

@implementation NennoIngredient

#pragma mark - JSONModel Methods
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{
              @"ingredientId"      : @"id",
              @"price"   : @"price",
              @"name"    : @"name",
              }];
}

#pragma mark - NSCoding Methods
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.ingredientId forKey:@"ingredientId"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.price forKey:@"price"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        self.ingredientId = [decoder decodeObjectForKey:@"ingredientId"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.price = [decoder decodeObjectForKey:@"price"];
    }
    return self;
}

@end
