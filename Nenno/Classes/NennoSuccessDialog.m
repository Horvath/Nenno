//
//  NennoSuccessDialog.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoSuccessDialog.h"

@interface NennoSuccessDialog ()

@property (nonatomic, weak) IBOutlet UIView *overlayView;

@end

@implementation NennoSuccessDialog

#pragma mark - initialization
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (!self)
    {
        return nil;
    }

    [self initView];

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (!self)
    {
        return nil;
    }

    [self initView];

    return self;
}

- (void)initView
{
    NSBundle *bundle = [NSBundle bundleForClass:self.class];
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:bundle];
    UIView *view = [nib instantiateWithOwner:self options:nil][0];

    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    [self addSubview:view];
    [self layoutIfNeeded];
}

#pragma mark - Utils
- (void)show
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];

    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.overlayView setAlpha:1];
                     }];
    [self performSelector:@selector(hide)
               withObject:nil
               afterDelay:3];
}

- (void)hide
{
    [UIView animateWithDuration:0.0 animations:^
    {
        [self.overlayView setAlpha:0];
    }
    completion:^(BOOL finished)
    {
        [self removeFromSuperview];
    }];
}

@end
