//
//  NennoPizzaItemDetailsViewController.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoBaseViewController.h"

@interface NennoPizzaItemDetailsViewController : NennoBaseViewController

@property (nonatomic, strong) NennoPizzaItem *pizzaItem;

@end
