//
//  NennoPizzasResponse.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol NennoPizzaItem

@end

@interface NennoPizzasResponse : JSONModel

#pragma mark - Default Properties
@property (nonatomic, strong) NSNumber *basePrice;
@property (nonatomic, strong) NSArray<NennoPizzaItem> *pizzaItems;

@end
