//
//  NennoDefaultButton.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoDefaultButton.h"

@implementation NennoDefaultButton

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];

    [UIView animateWithDuration:0.2f
                          delay:0.f
                        options:UIViewAnimationOptionAllowUserInteraction
    animations:^
    {
        self.backgroundColor =
        highlighted ? _highlitedColor : _defaultColor;
    }
    completion:nil];
}

@end
