//
//  NennoDefaultButton.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NennoDefaultButton : UIButton

@property (nonatomic, strong) IBInspectable UIColor *defaultColor;
@property (nonatomic, strong) IBInspectable UIColor *highlitedColor;

@end
