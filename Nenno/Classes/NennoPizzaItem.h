//
//  NennoPizzaItem.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <JSONModel/JSONModel.h>

#import "NennoCartItemProtocol.h"
#import "NennoIngredient.h"

@interface NennoPizzaItem : JSONModel <NSCopying, NSCoding, NennoCartItemProtocol>

#pragma mark - Default Properties
@property (nonatomic, strong) NSArray<NSNumber *> *ingredientIdentifiers;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSNumber *basePrice;
@property (nonatomic, assign) BOOL customPizza;

#pragma mark - Derived Properties
@property (nonatomic, strong) NSMutableArray<Optional,NennoIngredient *> *ingredients;
@property (nonatomic, strong) NSArray<Optional,NennoIngredient *> *defaultIngredients;
@property (nonatomic, strong) NSNumber *price;

+ (NennoPizzaItem *)customPizzaItem;

- (BOOL)containsIngredient:(NennoIngredient *)ingredient;
- (void)addIngredient:(NennoIngredient *)ingredient;
- (BOOL)canRemoveIngredient:(NennoIngredient *)ingredient;;
- (void)removeIngredient:(NennoIngredient *)ingredient;
- (void)resetIngredientsToDefault;
- (NSDictionary *)pizzaItemAsDictionary;

@end
