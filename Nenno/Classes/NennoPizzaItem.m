//
//  NennoPizzaItem.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoPizzaItem.h"

#import <ObjectiveSugar/ObjectiveSugar.h>

@implementation NennoPizzaItem

#pragma mark - JSONModel methods
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{
              @"ingredientIdentifiers" : @"ingredients",
              @"name"        : @"name",
              @"imageUrl"    : @"imageUrl",
              }];
}

+ (BOOL)propertyIsOptional:(NSString*)propertyName
{
    if ([propertyName isEqualToString: @"defaultIngredients"] ||
        [propertyName isEqualToString: @"price"] ||
        [propertyName isEqualToString: @"customPizza"] ||
        [propertyName isEqualToString:@"imageUrl"] ||
        [propertyName isEqualToString:@"basePrice"])
    {
        return YES;
    }

    return NO;
}

#pragma mark - Custom pizza item
+ (NennoPizzaItem *)customPizzaItem
{
    NennoPizzaItem *customPizzaItem = [NennoPizzaItem new];
    customPizzaItem.name = @"Custom pizza";
    customPizzaItem.ingredientIdentifiers = [NSArray new];
    customPizzaItem.ingredients = [NSMutableArray new];
    customPizzaItem.customPizza = YES;

    return customPizzaItem;
}

#pragma mark - Utils
- (NSNumber *)price
{
    __block NSInteger sumPrice = self.basePrice.integerValue;

    [_ingredients each:^(NennoIngredient *ingredient)
    {
        sumPrice += ingredient.price.integerValue;
    }];

    return [NSNumber numberWithInteger:sumPrice];
}

- (void)setIngredients:(NSMutableArray<NennoIngredient *> *)ingredients
{
    _ingredients = ingredients;
    self.defaultIngredients = [[NSArray alloc] initWithArray:ingredients copyItems:YES];
}

- (BOOL)containsIngredient:(NennoIngredient *)ingredient
{
    NennoIngredient *selectedIngredient =
    [_defaultIngredients find:^BOOL(NennoIngredient *defaultIngredient)
     {
         return ingredient.ingredientId.integerValue ==
         defaultIngredient.ingredientId.integerValue;
     }];

    BOOL contains = selectedIngredient != nil;
    return contains;
}

- (void)addIngredient:(NennoIngredient *)ingredient
{
    if (![_ingredients containsObject:ingredient])
    {
        [_ingredients addObject:ingredient];
    }
}

- (BOOL)canRemoveIngredient:(NennoIngredient *)ingredient
{
    return ![self containsIngredient:ingredient];
}

- (void)removeIngredient:(NennoIngredient *)ingredient
{
    [self.ingredients removeObject:ingredient];
}

- (void)resetIngredientsToDefault
{
    self.ingredients = [self.defaultIngredients mutableCopy];
}

- (NSDictionary *)pizzaItemAsDictionary
{
    return @{@"ingredients" : self.ingredientIdentifiers,
             @"name" : self.name,
             @"imageUrl" : self.imageUrl};
}

#pragma mark - NennoCartItemProtocol Methods
- (NSString *)cartItemName
{
    return self.name;
}

- (NSNumber *)cartItemPrice
{
    return self.price;
}

#pragma mark - NSCopying Methods
- (id)copyWithZone:(NSZone *)zone
{
    NennoPizzaItem *another = [[NennoPizzaItem alloc] init];

    another.name = [self.name copyWithZone:zone];
    another.ingredientIdentifiers = [self.ingredientIdentifiers copyWithZone:zone];
    another.imageUrl = [self.imageUrl copyWithZone:zone];
    another.ingredients = [self.ingredients copyWithZone:zone];
    another.defaultIngredients = [self.defaultIngredients copyWithZone:zone];
    another.basePrice = [self.basePrice copyWithZone:zone];

    return another;
}

#pragma mark - NSCoding Methods
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.ingredientIdentifiers forKey:@"ingredientIdentifiers"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [encoder encodeObject:self.ingredients forKey:@"ingredients"];
    [encoder encodeObject:self.defaultIngredients forKey:@"defaultIngredients"];
    [encoder encodeObject:self.basePrice forKey:@"basePrice"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        self.ingredientIdentifiers = [decoder decodeObjectForKey:@"ingredientIdentifiers"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.imageUrl = [decoder decodeObjectForKey:@"imageUrl"];
        self.ingredients = [decoder decodeObjectForKey:@"ingredients"];
        self.defaultIngredients = [decoder decodeObjectForKey:@"defaultIngredients"];
        self.basePrice = [decoder decodeObjectForKey:@"basePrice"];
    }
    return self;
}

@end
