//
//  UIColor+NennoColor.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (NennoColor)

+ (UIColor *)nennoRed;

@end
