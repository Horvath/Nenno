//
//  NennoNetworkManager.m
//
//  Created by Horváth Dávid on 2017. 09. 10..
//

#import "NennoNetworkManager.h"

#import <ObjectiveSugar/ObjectiveSugar.h>
#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>

@interface  NennoNetworkManager ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation NennoNetworkManager

- (instancetype)init
{
    self = [super init];

    if (!self)
    {
        return nil;
    }

    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    self.sessionManager = [AFHTTPSessionManager manager];
    _sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [_sessionManager.requestSerializer
     setValue:@"application/json"
     forHTTPHeaderField:@"Content-Type"];

    [[AFNetworkActivityLogger sharedLogger] startLogging];

    return self;
}

#pragma mark - CRUD Methods
- (void)postToPath:(NSString *)path
    withParameters:(NSDictionary *)parameters
   asResponseClass:(Class)responseClass
           success:(void (^)(id object))successBlock
           failure:(void (^)(NSError *error))failureBlock
{
    [_sessionManager POST:path
               parameters:parameters
                 progress:nil
                  success:^
     (NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         successBlock(responseObject);
     }
                  failure:
     ^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         failureBlock(error);
     }];
}

- (void)getObjectFromPath:(NSString *)path
                    class:(Class)responseClass
               withParams:(NSDictionary *)params
                  success:(void (^)(id result))successBlock
                  failure:(void (^)(NSError *error))failureBlock
{
    [_sessionManager GET:path
               parameters:params
                 progress:nil
                  success:^
     (NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         successBlock([self processResult:responseObject
                                 forClass:responseClass]);
     }
                  failure:
     ^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         failureBlock(error);
     }];
}

- (void)getObjectsArrayFromPath:(NSString *)path
                          class:(Class)responseClass
                     withParams:(NSDictionary *)params
                        success:(void (^)(NSArray *results))successBlock
                   failureBlock:(void (^)(NSError *error))failureBlock
{
    [_sessionManager GET:path
              parameters:params
                progress:nil
                 success:^
     (NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
             NSArray *results = [self processResults:responseObject forClass:responseClass];

             dispatch_async(dispatch_get_main_queue(), ^{
                 successBlock(results);
             });
         });
     }
    failure:
     ^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         failureBlock(error);
     }];
}

#pragma mark - Processing data with JSONModel
- (NSArray *)processResults:(NSArray *)results forClass:(Class)responseClass
{
    return [results map:^id(NSDictionary *result)
    {
        return [self processResult:result forClass:responseClass];
    }];
}

- (id)processResult:(NSDictionary *)result forClass:(Class)responseClass
{
    NSError *error;
    id instance = [[responseClass alloc] initWithDictionary:result error:&error];

    if (error)
    {
        NSLog(@"JSONModelError = %@", error);
    }

    return instance;
}

@end
