//
//  NennoPizzaItemDetailsViewController.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoPizzaItemDetailsViewController.h"

// ViewModels
#import "NennoIngredient.h"
#import "NennoPizzaItem.h"

// UI Components
#import "NennoIngredientTableViewCell.h"
#import "NennoDefaultButton.h"

// Helpers
#import <SDWebImage/UIImageView+WebCache.h>

@interface NennoPizzaItemDetailsViewController () <UITabBarDelegate, UITableViewDataSource>

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *ingredientsTableView;
@property (weak, nonatomic) IBOutlet UIImageView *pizzaImageView;
@property (weak, nonatomic) IBOutlet NennoDefaultButton *cartButton;

#pragma mark - Data Source
@property (nonatomic, strong) NSArray<NennoIngredient *> *ingredients;

@end

@implementation NennoPizzaItemDetailsViewController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [_pizzaItem resetIngredientsToDefault];
    [self setupPizzaItemAppearance];
    self.ingredients = self.nennoDataPresenter.ingredients;
}

#pragma mark - Setups
- (void)setupPizzaItemAppearance
{
    if (_pizzaItem.customPizza)
    {
        self.titleLabel.text = @"CREATE A PIZZA";
        [self.pizzaImageView setImage:[UIImage imageNamed:@"EmptyPizza"]];
    }
    else
    {
        self.titleLabel.text = [_pizzaItem.name uppercaseString];
        [self setupPizzaImageView];
    }

    [self refreshCartButtonCartButton];
}

- (void)setupPizzaImageView
{
    [_pizzaImageView setShowActivityIndicatorView:YES];
    [_pizzaImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];

    [_pizzaImageView sd_setImageWithURL:[NSURL URLWithString:_pizzaItem.imageUrl]
                       placeholderImage:[UIImage imageNamed:@"PizzaPlaceholder"]
                                options:SDWebImageProgressiveDownload];
}

- (void)refreshCartButtonCartButton
{
    [self.cartButton
     setTitle:[NSString stringWithFormat:@"ADD TO CART ($%ld)",
               (long)_pizzaItem.price.integerValue]
     forState:UIControlStateNormal];

    self.cartButton.alpha = (_pizzaItem.ingredients.count > 0) ? 1 : 0.6;
    self.cartButton.userInteractionEnabled = (_pizzaItem.ingredients.count > 0);
}

#pragma mark - UITableView Delegate / DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ingredients.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NennoIngredientTableViewCell *ingredientCell =
    [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([NennoIngredientTableViewCell class])];

    NennoIngredient *ingredient = _ingredients[indexPath.row];

    [ingredientCell setupWithIngredient:ingredient];

    ingredientCell.ingredientIsSelected = [_pizzaItem containsIngredient:ingredient];
    
    return ingredientCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NennoIngredientTableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];

    NennoIngredient *selectedIngredient = _ingredients[indexPath.row];

    BOOL shouldSelectIngredient = !selectedCell.ingredientIsSelected;

    if (shouldSelectIngredient)
    {
        [_pizzaItem addIngredient:selectedIngredient];
        selectedCell.ingredientIsSelected = shouldSelectIngredient;
    }
    else
    {
        if ([_pizzaItem canRemoveIngredient:selectedIngredient])
        {
            [_pizzaItem removeIngredient:selectedIngredient];
            selectedCell.ingredientIsSelected = shouldSelectIngredient;
        }
    }

    [self refreshCartButtonCartButton];
}

#pragma mark - Actions
- (IBAction)addToCartClicked:(id)sender
{
    [self.nennoDataPresenter addPizzaItemToCart:_pizzaItem];
    [self.nennoDataPresenter persistCart];

    [self showSnackBar];
}

@end
