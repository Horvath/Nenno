//
//  NennoNetworkManager.h
//
//  Created by Horváth Dávid on 2017. 09. 10..
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>

@interface NennoNetworkManager : NSObject

- (void)postToPath:(NSString *)path
    withParameters:(NSDictionary *)parameters
   asResponseClass:(Class)responseClass
           success:(void (^)(id object))successBlock
           failure:(void (^)(NSError *error))failureBlock;

- (void)getObjectFromPath:(NSString *)path
                    class:(Class)responseClass
               withParams:(NSDictionary *)params
                  success:(void (^)(id result))successBlock
                  failure:(void (^)(NSError *error))failureBlock;

- (void)getObjectsArrayFromPath:(NSString *)path
                          class:(Class)responseClass
                     withParams:(NSDictionary *)params
                        success:(void (^)(NSArray *results))successBlock
                   failureBlock:(void (^)(NSError *error))failureBlock;

@end
