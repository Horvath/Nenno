//
//  NennoPizzaItemTableViewCell.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoPizzaItemTableViewCell.h"

// ViewModel
#import "NennoPizzaItem.h"

// Helpers
#import <ObjectiveSugar/ObjectiveSugar.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface NennoPizzaItemTableViewCell ()

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *pizzaImageView;
@property (weak, nonatomic) IBOutlet UILabel *pizzaTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pizzaIngredientsLabel;
@property (weak, nonatomic) IBOutlet UIButton *pizzaCartButton;

@end

@implementation NennoPizzaItemTableViewCell

#pragma mark - Initialization
- (void)awakeFromNib
{
    [super awakeFromNib];

    UIImage *cartImage = [UIImage imageNamed:@"CartIcon"];
    cartImage = [cartImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    [self.pizzaCartButton setImage:cartImage forState:UIControlStateNormal];
    self.pizzaCartButton.imageView.tintColor = [UIColor whiteColor];
}

#pragma mark - Setups
- (void)setupWithPizzaItem:(NennoPizzaItem *)pizzaItem
{
    self.pizzaTitleLabel.text = pizzaItem.name;
    [self.pizzaCartButton setTitle:[NSString stringWithFormat:@"$%ld", (long)pizzaItem.price.integerValue]
                          forState:UIControlStateNormal];
    [self setupPizzaImageViewWithImageUrl:pizzaItem.imageUrl];
    [self setupIngredientsLabelWithIngredients:pizzaItem.ingredients];
}

- (void)setupIngredientsLabelWithIngredients:(NSArray<NennoIngredient *> *)ingredients
{
    NSMutableString *ingredientsString = [[NSMutableString alloc] initWithString:@""];

    [ingredients eachWithIndex:^(NennoIngredient *ingredient, NSUInteger index)
    {
        if (index == ingredients.count - 1)
        {
            [ingredientsString appendString:ingredient.name];
        }
        else
        {
            [ingredientsString appendString:[NSString stringWithFormat:@"%@, ", ingredient.name]];
        }
    }];

    self.pizzaIngredientsLabel.text = ingredientsString;
}

- (void)setupPizzaImageViewWithImageUrl:(NSString *)imageUrl
{
    [_pizzaImageView setShowActivityIndicatorView:YES];
    [_pizzaImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];

    [_pizzaImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                 placeholderImage:[UIImage imageNamed:@"PizzaPlaceholder"]
                          options:SDWebImageProgressiveDownload];
}

#pragma mark - Actions
- (IBAction)addToCartClicked:(id)sender
{
    if (_addToCartClicked)
    {
        _addToCartClicked(sender);
    }
}

@end
