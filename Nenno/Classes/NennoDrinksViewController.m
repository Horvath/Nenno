//
//  NennoDrinksViewController.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoDrinksViewController.h"

// ViewModel
#import "NennoDrinkItem.h"

// UI Components
#import "NennoDrinkTableViewCell.h"

@interface NennoDrinksViewController () <UITableViewDelegate, UITableViewDataSource>

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UITableView *drinksTableView;

#pragma mark - Data Source
@property (nonatomic, strong) NSArray<NennoDrinkItem *> *drinks;

@end

@implementation NennoDrinksViewController

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.drinks = self.nennoDataPresenter.drinks;
}

#pragma mark - UITableView Delegate / DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.drinks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NennoDrinkTableViewCell *drinkItemCell =
    [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([NennoDrinkTableViewCell class])];

    NennoDrinkItem *drinkItem = _drinks[indexPath.row];

    [drinkItemCell setupWithDrink:drinkItem];

    [drinkItemCell setAddItemClicked:^(id sender)
    {
        [self.nennoDataPresenter addDrinkItemToCart:drinkItem];
        [self showSnackBar];
    }];

    return drinkItemCell;
}

@end
