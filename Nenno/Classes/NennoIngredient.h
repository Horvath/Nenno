//
//  NennoIngredient.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface NennoIngredient : JSONModel <NSCoding>

#pragma mark - Default Properties
@property (nonatomic, strong) NSNumber *ingredientId;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSString *name;

@end
