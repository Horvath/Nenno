//
//  NennoIngredientTableViewCell.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NennoIngredient;

@interface NennoIngredientTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL ingredientIsSelected;

- (void)setupWithIngredient:(NennoIngredient *)ingredient;

@end
