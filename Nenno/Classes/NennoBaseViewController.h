//
//  NennoBaseViewController.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 08..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NennoModule.h"
#import "NennoDataPresenterInterface.h"

@interface NennoBaseViewController : UIViewController

#pragma mark - Injected Poperties
@property (weak, nonatomic) InjectedClass(NennoModule) nennoModule;
@property (weak, nonatomic) InjectedProtocol(NennoDataPresenterInterface) nennoDataPresenter;

- (void)showSnackBar;
- (void)handleError:(NSError *)error;
- (void)presentLoading;
- (void)hideLoading;

@end
