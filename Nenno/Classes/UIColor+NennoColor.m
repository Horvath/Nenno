//
//  UIColor+NennoColor.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "UIColor+NennoColor.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBA(rgbaValue) [UIColor colorWithRed:((float)((rgbaValue & 0xFF000000) >> 24))/255.0 green:((float)((rgbaValue & 0xFF0000) >> 16))/255.0 blue:((float)((rgbaValue & 0xFF00) >> 8))/255.0 alpha:((float)(rgbaValue & 0xFF))/255.0];

@implementation UIColor (NennoColor)

+ (UIColor *)nennoRed
{
    return UIColorFromRGB(0xE14D45);
}

@end
