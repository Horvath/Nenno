//
//  NennoCartItemCell.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

// ViewModel
@class NennoPizzaItem;
@protocol NennoCartItemProtocol;

@interface NennoCartItemCell : UITableViewCell

@property (nonatomic, copy) void (^removeItemClicked)(id);

- (void)setupWithCartItem:(id<NennoCartItemProtocol>)cartItem;

@end
