//
//  NennoPizzasResponse.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoPizzasResponse.h"

@implementation NennoPizzasResponse

#pragma mark - JSONModel Methods
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{
              @"basePrice"     : @"basePrice",
              @"pizzaItems"    : @"pizzas",
              }];
}

@end
