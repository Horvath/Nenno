//
//  NennoIngredientTableViewCell.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoIngredientTableViewCell.h"

#import "NennoIngredient.h"

@interface NennoIngredientTableViewCell ()

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UILabel *ingredientTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ingredientPriceLabel;

@end

@implementation NennoIngredientTableViewCell

#pragma mark - Setups
- (void)setupWithIngredient:(NennoIngredient *)ingredient
{
    self.ingredientTitleLabel.text = ingredient.name;
    self.ingredientPriceLabel.text =
    [NSString stringWithFormat:@"$%ld", ingredient.price.integerValue];
}

#pragma mark - Utils
- (void)setIngredientIsSelected:(BOOL)ingredientIsSelected
{
    _ingredientIsSelected = ingredientIsSelected;
    self.selectedImageView.highlighted = ingredientIsSelected;
}

@end
