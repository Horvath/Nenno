//
//  NennoCartItemProtocol.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

@protocol NennoCartItemProtocol <NSObject>

@required
- (NSString *)cartItemName;
- (NSNumber *)cartItemPrice;

@end