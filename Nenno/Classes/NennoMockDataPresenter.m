//
//  NennoMockDataPresenter.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoMockDataPresenter.h"

// ViewModel
#import "NennoIngredient.h"
#import "NennoPizzaItem.h"
#import "NennoDrinkItem.h"

// Helpers
#import <ObjectiveSugar/ObjectiveSugar.h>

@implementation NennoMockDataPresenter

@synthesize ingredients, cart, cartTotalPrice, drinks;

#pragma mark - Ingredients
- (void)loadIngredients:(void (^)(NSArray<NennoIngredient *> *))successBlock
             errorBlock:(void (^)())errorBlock
{
    self.ingredients = [self createMockIngredients];

    successBlock(self.ingredients);
}

#pragma mark - Drinks
- (void)loadDrinks:(void (^)(NSArray<NennoDrinkItem *> *))successBlock
        errorBlock:(void (^)())errorBlock
{
    self.drinks = [self createMockDrinkItems];

    successBlock(self.drinks);
}

#pragma mark - Pizzas
- (void)loadPizzas:(void (^)(NSArray<NennoPizzaItem *> *))successBlock
        errorBlock:(void (^)(NSError *))errorBlock
{
    [self loadIngredients:^(NSArray<NennoIngredient *> *ingredients)
     {
         [self loadDrinks:^(NSArray<NennoDrinkItem *> *drinks)
          {
             NSArray<NennoPizzaItem *> *pizzaItems = [self createMockPizzaItems];

             [pizzaItems each:^(NennoPizzaItem *pizzaItem)
              {
                  [self mapIngredientsToPizzaItem:pizzaItem basePrice:@5];
              }];

             successBlock(pizzaItems);
         }
        errorBlock:^(NSError *error)
         {
         }];
     }
    errorBlock:^(NSError *error)
     {
     }];
}

- (void)checkout:(void (^)())successBlock
      errorBlock:(void (^)(NSError *))errorBlock
{
    successBlock();
}

#pragma mark - Mock Data
- (NSArray<NennoIngredient *> *)createMockIngredients
{
    NennoIngredient *ingredient1 = [NennoIngredient new];
    ingredient1.ingredientId = @1;
    ingredient1.price = @1;
    ingredient1.name = @"Mozzarella";

    NennoIngredient *ingredient2 = [NennoIngredient new];
    ingredient2.ingredientId = @2;
    ingredient2.price = @1;
    ingredient2.name = @"Tomato Sauce";

    NennoIngredient *ingredient3 = [NennoIngredient new];
    ingredient3.ingredientId = @3;
    ingredient3.price = @1;
    ingredient3.name = @"Mushrooms";

    NennoIngredient *ingredient4 = [NennoIngredient new];
    ingredient4.ingredientId = @4;
    ingredient4.price = @1;
    ingredient4.name = @"Ricci";

    NennoIngredient *ingredient5 = [NennoIngredient new];
    ingredient5.ingredientId = @5;
    ingredient5.price = @1;
    ingredient5.name = @"Salami";

    NennoIngredient *ingredient6 = [NennoIngredient new];
    ingredient6.ingredientId = @6;
    ingredient6.price = @1;
    ingredient6.name = @"Asparagus";

    NennoIngredient *ingredient7 = [NennoIngredient new];
    ingredient7.ingredientId = @7;
    ingredient7.price = @1;
    ingredient7.name = @"Pineapple";

    return @[ingredient1, ingredient2, ingredient3, ingredient4, ingredient5, ingredient6, ingredient7];
}

- (NSArray<NennoPizzaItem *> *)createMockPizzaItems
{
    NennoPizzaItem *pizza1 = [NennoPizzaItem new];
    pizza1.ingredientIdentifiers = @[@1, @2];
    pizza1.name = @"Margherita";
    pizza1.imageUrl = @"https://cdn.pbrd.co/images/tOhJQ5N3.png";

    NennoPizzaItem *pizza2 = [NennoPizzaItem new];
    pizza2.ingredientIdentifiers = @[@1, @5];
    pizza2.name = @"Ricci";
    pizza2.imageUrl = @"https://cdn.pbrd.co/images/M57VcfLGQ.png";

    NennoPizzaItem *pizza3 = [NennoPizzaItem new];
    pizza3.ingredientIdentifiers = @[@7, @2];
    pizza3.name = @"Boscaiola";
    pizza3.imageUrl = @"https://cdn.pbrd.co/images/M57VcfLGQ.png";

    NennoPizzaItem *pizza4 = [NennoPizzaItem new];
    pizza4.ingredientIdentifiers = @[@3, @4];
    pizza4.name = @"Primavera";
    pizza4.imageUrl = @"https://cdn.pbrd.co/images/M58jWCFVC.png";

    NennoPizzaItem *pizza5 = [NennoPizzaItem new];
    pizza5.ingredientIdentifiers = @[@5, @6];
    pizza5.name = @"Hawaii";
    pizza5.imageUrl = @"https://cdn.pbrd.co/images/M57VcfLGQ.png";

    NennoPizzaItem *pizza6 = [NennoPizzaItem new];
    pizza6.ingredientIdentifiers = @[@1, @5];
    pizza6.name = @"Mare Bianco";
    pizza6.imageUrl = @"https://cdn.pbrd.co/images/4O6T9RQLX.png";

    return @[pizza1, pizza2, pizza3, pizza4, pizza5, pizza6];
}

- (NSArray<NennoDrinkItem *> *)createMockDrinkItems
{
    NennoDrinkItem *drink1 = [NennoDrinkItem new];
    drink1.drinkId = @1;
    drink1.name = @"Still Water";
    drink1.price = @2;

    NennoDrinkItem *drink2 = [NennoDrinkItem new];
    drink2.drinkId = @2;
    drink2.name = @"Sparkling Water";
    drink2.price = @4;

    NennoDrinkItem *drink3 = [NennoDrinkItem new];
    drink3.drinkId = @3;
    drink3.name = @"Coke";
    drink3.price = @1;

    NennoDrinkItem *drink4 = [NennoDrinkItem new];
    drink4.drinkId = @4;
    drink4.name = @"Beer";
    drink4.price = @3;

    return @[drink1, drink2, drink3, drink4];
}

#pragma mark - Utils
- (void)mapIngredientsToPizzaItem:(NennoPizzaItem *)pizzaItem
                        basePrice:(NSNumber *)basePrice
{
    NSArray *mappedIngredients =
    [pizzaItem.ingredientIdentifiers map:^id(NSNumber *ingredientIdentifier)
     {
         return [self.ingredients find:^BOOL(NennoIngredient *ingredient)
                 {
                     return ingredient.ingredientId.integerValue == ingredientIdentifier.integerValue;
                 }];
     }];

    pizzaItem.ingredients = [NSMutableArray arrayWithArray:mappedIngredients];
    pizzaItem.basePrice = basePrice;
}

- (void)addPizzaItemToCart:(NennoPizzaItem *)pizzaItem
{
    if (!self.cart)
    {
        self.cart = [NSMutableArray new];
    }

    [self.cart addObject:[pizzaItem copy]];
}

- (void)addDrinkItemToCart:(NennoDrinkItem *)drinkItem
{
    if (!self.cart)
    {
        self.cart = [NSMutableArray new];
    }

    [self.cart addObject:drinkItem];
}

- (void)deleteCartItemFromCart:(NSInteger)index
{
    [self.cart removeObjectAtIndex:index];
}

- (void)persistCart
{
    // Do nothing in mock!
}

- (NSNumber *)cartTotalPrice
{
    __block NSInteger sumPrice = 0;

    [self.cart each:^(NennoPizzaItem *pizzaItem)
     {
         sumPrice += pizzaItem.price.integerValue;
     }];

    return [NSNumber numberWithInteger:sumPrice];
}

@end
