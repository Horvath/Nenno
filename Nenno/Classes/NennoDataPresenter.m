//
//  NennoDataPresenter.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoDataPresenter.h"

// Network
#import "NennoNetworkManager.h"

// ViewModel
#import "NennoIngredient.h"
#import "NennoPizzaItem.h"
#import "NennoDrinkItem.h"
#import "NennoPizzasResponse.h"

// Helpers
#import <ObjectiveSugar/ObjectiveSugar.h>

static NSString * const kIngredientsEndpoint
= @"https://beta.json-generator.com/api/json/get/EkTFDCdsG";
static NSString * const kDrinksEndpoint
= @"https://beta.json-generator.com/api/json/get/N1mnOA_oz";
static NSString * const kPizzasEndpoint
= @"https://beta.json-generator.com/api/json/get/NybelGcjz";
static NSString * const kCheckoutEndpoint
= @"http://posttestserver.com/post.php";

@interface NennoDataPresenter ()

@property (nonatomic, strong) NennoNetworkManager *networkManager;

@end

@implementation NennoDataPresenter

@synthesize ingredients, cart, cartTotalPrice, drinks;

#pragma mark - Initialization
- (instancetype)init
{
    self = [super init];

    if (!self)
    {
        return nil;
    }

    self.networkManager = [NennoNetworkManager new];

    return self;
}

#pragma mark - Drinks
- (void)loadDrinks:(void (^)(NSArray<NennoDrinkItem *> *))successBlock
        errorBlock:(void (^)())errorBlock
{
    [self.networkManager getObjectsArrayFromPath:kDrinksEndpoint
                                           class:[NennoDrinkItem class]
                                      withParams:nil
    success:^(NSArray<NennoDrinkItem *> *fetchedDrinks)
     {
         self.drinks = fetchedDrinks;
         successBlock(fetchedDrinks);
     }
    failureBlock:errorBlock];
}

#pragma mark - Ingredients
- (void)loadIngredients:(void (^)(NSArray<NennoIngredient *> *))successBlock
             errorBlock:(void (^)(NSError *))errorBlock
{
    [self.networkManager getObjectsArrayFromPath:kIngredientsEndpoint
                                           class:[NennoIngredient class]
                                      withParams:nil
    success:^(NSArray<NennoIngredient *> *fetchedIngredients)
     {
         self.ingredients = fetchedIngredients;
         successBlock(ingredients);
     }
    failureBlock:errorBlock];
}

#pragma mark - Pizzas
- (void)loadPizzas:(void (^)(NSArray<NennoPizzaItem *> *))successBlock
        errorBlock:(void (^)(NSError *))errorBlock
{
    [self loadDrinks:^(NSArray<NennoDrinkItem *> *drinks)
    {
        [self loadIngredients:^(NSArray<NennoIngredient *> *ingredients)
        {
            [self.networkManager getObjectFromPath:kPizzasEndpoint
                                             class:[NennoPizzasResponse class]
                                        withParams:nil
                success:^(NennoPizzasResponse *pizzasResponse)
            {
                NSArray<NennoPizzaItem *> *pizzaItems = pizzasResponse.pizzaItems;

                [pizzaItems each:^(NennoPizzaItem *pizzaItem)
                 {
                     [self mapIngredientsToPizzaItem:pizzaItem
                                           basePrice:pizzasResponse.basePrice];
                 }];

                successBlock(pizzaItems);
                self.cart = [self loadCartFromCache];
            }
                failure:^(NSError *error)
            {

            }];
        }
                   errorBlock:errorBlock];
    }
          errorBlock:errorBlock];
}

#pragma mark - Checkout
- (void)checkout:(void (^)())successBlock
        errorBlock:(void (^)(NSError *))errorBlock
{
    NSDictionary *params = @{@"pizzas" : [self getPizzaItemsFromCart],
                             @"drinks" : [self getDrinkIdentifiersFromCart]};

    [self.networkManager postToPath:kCheckoutEndpoint
                     withParameters:params
                    asResponseClass:nil
                            success:^(id object)
    {
        successBlock();
    }
    failure:errorBlock];
}

#pragma mark - Utils
- (void)mapIngredientsToPizzaItem:(NennoPizzaItem *)pizzaItem
                        basePrice:(NSNumber *)basePrice
{
    NSArray *mappedIngredients =
    [pizzaItem.ingredientIdentifiers map:^id(NSNumber *ingredientIdentifier)
     {
         return [self.ingredients find:^BOOL(NennoIngredient *ingredient)
                 {
                     return ingredient.ingredientId.integerValue == ingredientIdentifier.integerValue;
                 }];
     }];

    pizzaItem.ingredients = [NSMutableArray arrayWithArray:mappedIngredients];
    pizzaItem.basePrice = basePrice;
}

- (void)addPizzaItemToCart:(NennoPizzaItem *)pizzaItem
{
    if (!self.cart)
    {
        self.cart = [NSMutableArray new];
    }

    [self.cart addObject:[pizzaItem copy]];
}

- (void)addDrinkItemToCart:(NennoDrinkItem *)drinkItem
{
    if (!self.cart)
    {
        self.cart = [NSMutableArray new];
    }

    [self.cart addObject:drinkItem];
}

- (void)deleteCartItemFromCart:(NSInteger)index
{
    [self.cart removeObjectAtIndex:index];
}

- (NSNumber *)cartTotalPrice
{
    __block NSInteger sumPrice = 0;

    [self.cart each:^(NennoPizzaItem *pizzaItem)
     {
         sumPrice += pizzaItem.price.integerValue;
     }];

    return [NSNumber numberWithInteger:sumPrice];
}

- (NSArray<NSNumber *> *)getDrinkIdentifiersFromCart
{
    NSMutableArray *drinkIds = [NSMutableArray new];

    [self.cart each:^(id<NennoCartItemProtocol> cartItem)
    {
        if ([cartItem isKindOfClass:[NennoDrinkItem class]])
        {
            [drinkIds addObject:((NennoDrinkItem *)cartItem).drinkId];
        }
    }];

    return drinkIds;
}

- (NSArray<NSDictionary *> *)getPizzaItemsFromCart
{
    NSMutableArray *pizzaItems = [NSMutableArray new];

    [self.cart each:^(id<NennoCartItemProtocol> cartItem)
     {
         if ([cartItem isKindOfClass:[NennoPizzaItem class]])
         {
             [pizzaItems addObject:[((NennoPizzaItem *)cartItem) pizzaItemAsDictionary]];
             NSLog(@"%@",[((NennoPizzaItem *)cartItem) pizzaItemAsDictionary]);
         }
     }];

    return pizzaItems;
}

#pragma mark - Persist Cart
- (void)persistCart
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"cart.txt"];

    NSMutableArray *myObject = [NSMutableArray array];
    [myObject addObjectsFromArray:self.cart];

    [NSKeyedArchiver archiveRootObject:myObject toFile:appFile];
}

- (NSMutableArray<id<NennoCartItemProtocol>> *)loadCartFromCache
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"cart.txt"];

    NSMutableArray* myArray = [NSKeyedUnarchiver unarchiveObjectWithFile:appFile];

    return myArray;
}

@end
