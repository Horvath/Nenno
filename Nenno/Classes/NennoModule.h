//
//  NennoModule.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "Typhoon.h"

#import "NennoDataPresenterInterface.h"

@class NennoSnackBarView;

@interface NennoModule : TyphoonAssembly

- (id<NennoDataPresenterInterface>)nennoDataPresenter;

@end
