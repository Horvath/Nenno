//
//  NennoCartViewController.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoCartViewController.h"

// UI Components
#import "NennoCartItemCell.h"
#import "NennoSuccessDialog.h"

// ViewModel
#import "NennoPizzaItem.h"
#import "NennoCartItemProtocol.h"

@interface NennoCartViewController () <UITableViewDelegate, UITableViewDataSource>

#pragma mark - UI Components
@property (weak, nonatomic) IBOutlet UITableView *cartTableView;
@property (weak, nonatomic) IBOutlet UILabel *cartTotalPriceLabel;
@property (nonatomic, strong) NennoSuccessDialog *successDialog;

#pragma mark - Data Source
@property (nonatomic, strong) NSArray<id<NennoCartItemProtocol>> *cart;

@end

@implementation NennoCartViewController

#pragma mark - Life Cycle
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.cart = self.nennoDataPresenter.cart;
    self.successDialog = [[NennoSuccessDialog alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self setupCart];
    [self.cartTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self.nennoDataPresenter persistCart];
}

- (void)setupCart
{
    self.cartTotalPriceLabel.text =
    [NSString stringWithFormat:@"$%ld",
     (long)self.nennoDataPresenter.cartTotalPrice.integerValue];
}

#pragma mark - UITableView Delegate / DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cart.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NennoCartItemCell *cartItemCell =
    [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([NennoCartItemCell class])];

    [cartItemCell setupWithCartItem:_cart[indexPath.row]];

    [cartItemCell setRemoveItemClicked:^(id sender)
    {
        [self tableView:tableView
     commitEditingStyle:UITableViewCellEditingStyleDelete
      forRowAtIndexPath:indexPath];
    }];

    return cartItemCell;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self.nennoDataPresenter deleteCartItemFromCart:indexPath.row];

        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];

        [tableView reloadData];
        [self setupCart];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - Actions
- (IBAction)checkoutClicked:(id)sender
{
    [self.successDialog show];

    [self.nennoDataPresenter checkout:^
    {
    }
    errorBlock:^(NSError *error)
    {
        [self handleError:error];
    }];
}

@end
