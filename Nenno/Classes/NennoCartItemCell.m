//
//  NennoCartItemCell.m
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 09..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import "NennoCartItemCell.h"

#import "NennoCartItemProtocol.h"

@interface NennoCartItemCell ()

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UILabel *cartItemTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cartItemPriceLabel;

@end

@implementation NennoCartItemCell

#pragma mark - Setups
- (void)setupWithCartItem:(id<NennoCartItemProtocol>)cartItem
{
    self.cartItemTitleLabel.text = [cartItem cartItemName];
    self.cartItemPriceLabel.text =
    [NSString stringWithFormat:@"$%ld", [cartItem cartItemPrice].integerValue];
}

#pragma mark - Actions
- (IBAction)removeItemClicked:(id)sender
{
    if (_removeItemClicked)
    {
        _removeItemClicked(sender);
    }
}

@end
