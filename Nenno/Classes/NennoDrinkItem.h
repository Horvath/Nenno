//
//  NennoDrinkItem.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 10..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "NennoCartItemProtocol.h"

@interface NennoDrinkItem : JSONModel <NennoCartItemProtocol, NSCoding>

#pragma mark - Default Properties
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *drinkId;

@end
