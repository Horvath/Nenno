//
//  AppDelegate.h
//  Nenno
//
//  Created by Horváth Dávid on 2017. 09. 08..
//  Copyright © 2017. David Horvath. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

